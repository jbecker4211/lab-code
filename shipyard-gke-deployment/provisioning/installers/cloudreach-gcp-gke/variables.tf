##########################################
########### VARIABLES FOR GKE and BASTION
##########################################

variable "cloudreach_region" {
  description = "region to deploy to"
  type        = "string"
}

variable "cloudreach_firewall_source_ranges" {
  description = "list of ip ranges to allow traffic from"
  type        = "list"
}

variable "cloudreach_bastion_metadata" {
  description = "metadata map for instances"
  type        = "map"
}

######################################################
############ Variables for bastion network
######################################################

variable "cloudreach_subnet_region_bastion" {
  description = "regions to deploy bastion networks too"
  type        = "list"
}

variable "cloudreach_subnet_ip_cidr_range_bastion" {
  description = "cidr range for subnet"
  type        = "string"
}

#####################################################
################ Variables for prod subnet
#####################################################

variable "cloudreach_subnet_region_production" {
  description = "production regions"
  type        = "list"
}

variable "cloudreach_subnet_ip_cidr_range_production" {
  description = "/16 for production traffic"
  type        = "string"
}

variable "cloudreach_project" {
  type        = "string"
  description = "project to deploy to"
}

variable "cloudreach_k8_username" {
  type        = "string"
  description = "master username"
}

variable "cloudreach_k8_password" {
  type        = "string"
  description = "password for master"
}

variable "cloudreach_k8_master_authorized_blocks" {
  type        = "string"
  description = "list of cidr block to allow traffic from.  to make cluster private omit line"
}

variable "cloudreach_nat_metadata" {
  type        = "map"
  description = "metadata for nat gateway"
}
