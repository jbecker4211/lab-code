module "cloudreach-gcp-gke" {
  source                                     = "../../modules/cloudreach-gcp-gke"
  cloudreach_project                         = "${var.cloudreach_project}"
  cloudreach_region                          = "${var.cloudreach_region}"
  cloudreach_firewall_source_ranges          = "${var.cloudreach_firewall_source_ranges}"
  cloudreach_bastion_metadata                = "${var.cloudreach_bastion_metadata}"
  cloudreach_subnet_region_bastion           = "${var.cloudreach_subnet_region_bastion}"
  cloudreach_subnet_ip_cidr_range_bastion    = "${var.cloudreach_subnet_ip_cidr_range_bastion}"
  cloudreach_subnet_region_production        = "${var.cloudreach_subnet_region_production}"
  cloudreach_subnet_ip_cidr_range_production = "${var.cloudreach_subnet_ip_cidr_range_production}"
  cloudreach_k8_username                     = "${var.cloudreach_k8_username}"
  cloudreach_k8_password                     = "${var.cloudreach_k8_password}"
  cloudreach_k8_master_authorized_blocks     = "${var.cloudreach_k8_master_authorized_blocks}"
  cloudreach_nat_metadata                    = "${var.cloudreach_nat_metadata}"
}



terraform{
backend "gcs" {
bucket = "remote-state-bucket-lab23"
credentials = "/Users/jamesbecker/crlab.json"
}


}

provider "google" {
  credentials = "${file("/Users/jamesbecker/crlab.json")}"
  project     = "${var.cloudreach_project}"
  region      = "${var.cloudreach_region}"
}


