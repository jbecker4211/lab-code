
resource "google_project_iam_binding" "log-writer" {
  project = "${var.cloudreach_project}"
  role    = "roles/logging.logWriter"
  members = ["serviceAccount:${google_service_account.k8-service-account.email}", "serviceAccount:${google_service_account.nat-service-account.email}", "serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_project_iam_binding" "metric-writer" {
  project = "${var.cloudreach_project}"
  role    = "roles/monitoring.metricWriter"
  members = ["serviceAccount:${google_service_account.k8-service-account.email}", "serviceAccount:${google_service_account.nat-service-account.email}", "serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_project_iam_binding" "monitoring" {
  project = "${var.cloudreach_project}"
  role    = "roles/monitoring.viewer"
  members = ["serviceAccount:${google_service_account.k8-service-account.email}", "serviceAccount:${google_service_account.nat-service-account.email}", "serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_project_iam_binding" "object-read" {
project = "${var.cloudreach_project}"
role = "roles/storage.objectViewer"
members = ["serviceAccount:${google_service_account.k8-service-account.email}","serviceAccount:${google_service_account.kubernetes.email}", "serviceAccount:${google_service_account.bastion-service-account.email}"]
}


resource "google_project_iam_binding" "gke-access" {
project = "${var.cloudreach_project}"
role = "roles/container.admin"
members = ["serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_project_iam_binding" "source-repo-access" {
project = "${var.cloudreach_project}"
role = "roles/source.writer"
members = ["serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_project_iam_binding" "storage-write"{
project = "${var.cloudreach_project}"
role = "roles/storage.admin"
members = ["serviceAccount:${google_service_account.bastion-service-account.email}"]
}

resource "google_service_account" "k8-service-account" {
  account_id   = "kubernetes-cluster-instance"
  display_name = "kubernetes-cluster-instance"
  project      = "${var.cloudreach_project}"
}

resource "google_service_account" "nat-service-account" {
  account_id   = "nat-gateway"
  display_name = "nat-gateway"
  project      = "${var.cloudreach_project}"
}

resource "google_service_account" "bastion-service-account" {
  account_id   = "bastion"
  display_name = "bastion"
  project      = "${var.cloudreach_project}"
}

resource "google_service_account" "kubernetes" {
    account_id = "kubernetes"
    display_name = "kubernetes"
    project = "${var.cloudreach_project}"
}
