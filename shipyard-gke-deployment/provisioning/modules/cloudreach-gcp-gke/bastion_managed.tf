#### INSTANCE TEMPLATE FOR BASTION HOST

data "template_file" "bastion-startup-script" {
  template = "${file("${format("%s/files/bastion.sh",path.module)}")}"
}

resource "google_compute_instance_template" "bastion-template" {
  name_prefix = "bastion-template-"
  description = "template for bastion host"
  tags        = ["bastion", "external"]

  lifecycle {
    create_before_destroy = true
  }

  disk {
    auto_delete  = "true"
    source_image = "centos-cloud/centos-7"
    mode         = "READ_WRITE"
    disk_type    = "pd-ssd"
    disk_size_gb = "25"
  }

  machine_type            = "n1-standard-1"
  can_ip_forward          = "false"
  instance_description    = "bastion instance"
  metadata                = "${var.cloudreach_bastion_metadata}"
  metadata_startup_script = "${data.template_file.bastion-startup-script.rendered}"

  network_interface {
    subnetwork = "${google_compute_subnetwork.bastion_subnetworks.0.self_link}"

    access_config {
      network_tier = "PREMIUM"
    }
  }

  service_account {
    scopes = ["cloud-platform"]
    email  = "${google_service_account.bastion-service-account.email}"
  }

  project = "${var.cloudreach_project}"
  region  = "${var.cloudreach_region}"

  scheduling {
    automatic_restart   = "true"
    on_host_maintenance = "MIGRATE"
    preemptible         = "false"
  }
}

#### INSTANCE GROUP MANAGER

resource "google_compute_region_instance_group_manager" "bastion-manager" {
  name               = "bastion-manager"
  base_instance_name = "bastion-k8"
  instance_template  = "${google_compute_instance_template.bastion-template.self_link}"
  region             = "${var.cloudreach_region}"
  project            = "${var.cloudreach_project}"
  update_strategy    = "ROLLING_UPDATE"

  rolling_update_policy {
    type           = "PROACTIVE"
    minimal_action = "RESTART"
  }

  target_size = "1"
}
