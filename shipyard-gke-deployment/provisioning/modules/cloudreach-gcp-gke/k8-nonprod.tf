resource "google_container_cluster" "nonprod" {
  name             = "kubernetes-cluster-nonprod"
  zone             = "us-central1-a"
  additional_zones = ["us-central1-b", "us-central1-c"]

  addons_config {
    horizontal_pod_autoscaling {
      disabled = "false"
    }

    http_load_balancing {
      disabled = "false"
    }

    kubernetes_dashboard {
      disabled = "false"
    }

    network_policy_config {
      disabled = "false"
    }
  }

  master_auth {
    username = "${var.cloudreach_k8_username}"
    password = "${var.cloudreach_k8_password}"
  }

  initial_node_count      = "1"
  description             = "kubernetes cluster"
  enable_kubernetes_alpha = "false"
  enable_legacy_abac      = "false"

  ip_allocation_policy {
    cluster_secondary_range_name  = "${lookup(google_compute_subnetwork.clustersubnetwork-nonprod.secondary_ip_range[0], "range_name")}"
    services_secondary_range_name = "${lookup(google_compute_subnetwork.clustersubnetwork-nonprod.secondary_ip_range[1],"range_name")}"
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "03:00"
    }
  }

  master_authorized_networks_config {}

  master_ipv4_cidr_block = "10.10.0.0/28"
  network                = "${google_compute_network.prod-network.self_link}"

  network_policy {
    provider = "PROVIDER_UNSPECIFIED"
    enabled  = "false"
  }

  pod_security_policy_config {
    enabled = "false"
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    service_account = "${google_service_account.k8-service-account.email}"
    tags            = ["bastionmanaged", "nat"]
  }

  private_cluster          = "true"
  project                  = "${var.cloudreach_project}"
  remove_default_node_pool = "false"
  subnetwork               = "${google_compute_subnetwork.clustersubnetwork-nonprod.self_link}"
}
