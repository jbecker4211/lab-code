resource "google_compute_network" "prod-network" {
  name                    = "prod-network"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
  description             = "Production Network"
  project                 = "${var.cloudreach_project}"
}

resource "google_compute_network" "bastion-network" {
  name                    = "bastion-vpc"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
  description             = "Bastion Network"
  project                 = "${var.cloudreach_project}"
}

resource "google_compute_network_peering" "peera" {
  name         = "${google_compute_network.prod-network.name}-peering"
  network      = "${google_compute_network.prod-network.self_link}"
  peer_network = "${google_compute_network.bastion-network.self_link}"

  auto_create_routes = "true"
}

resource "google_compute_network_peering" "peerb" {
  name         = "${google_compute_network.bastion-network.name}-peering"
  network      = "${google_compute_network.bastion-network.self_link}"
  peer_network = "${google_compute_network.prod-network.self_link}"

  auto_create_routes = "true"
}

resource "google_compute_subnetwork" "prod_subnetworks" {
  count                    = 9
  name                     = "production-${element(var.cloudreach_subnet_region_production,count.index%3)}-${count.index+1}"
  ip_cidr_range            = "${element(split(".",var.cloudreach_subnet_ip_cidr_range_production),0)}.${element(split(".",var.cloudreach_subnet_ip_cidr_range_production),1)}.${count.index}.0/24"
  network                  = "${google_compute_network.prod-network.self_link}"
  region                   = "${element(var.cloudreach_subnet_region_production,count.index%3)}"
  description              = "production subnet ${count.index}"
  project                  = "${var.cloudreach_project}"
  private_ip_google_access = "true"
  enable_flow_logs         = "false"
}

resource "google_compute_subnetwork" "bastion_subnetworks" {
  count                    = 3
  name                     = "bastion-${count.index+1}"
  ip_cidr_range            = "${element(split(".",var.cloudreach_subnet_ip_cidr_range_bastion),0)}.${element(split(".",var.cloudreach_subnet_ip_cidr_range_bastion),1)}.1.${count.index*16}/28"
  network                  = "${google_compute_network.bastion-network.self_link}"
  region                   = "${element(var.cloudreach_subnet_region_bastion,count.index)}"
  description              = "bastion subnet ${count.index}"
  project                  = "${var.cloudreach_project}"
  private_ip_google_access = "true"
  enable_flow_logs         = "false"
}

resource "google_compute_subnetwork" "clustersubnetwork" {
  name                     = "kubernetes-subneti-prod"
  ip_cidr_range            = "10.2.0.0/16"
  network                  = "${google_compute_network.prod-network.self_link}"
  region                   = "${var.cloudreach_region}"
  description              = "subnet to deploy kubernetes cluster to"
  project                  = "${var.cloudreach_project}"
  private_ip_google_access = "true"
  enable_flow_logs         = "false"

  secondary_ip_range = [{
    "ip_cidr_range" = "10.4.0.0/16"

    "range_name" = "secondary1"
  },
    {
      "ip_cidr_range" = "10.3.0.0/16"

      "range_name" = "secondary2"
    },
  ]
}


resource "google_compute_subnetwork" "clustersubnetwork-nonprod" {
  name                     = "kubernetes-subnet-nonprod"
  ip_cidr_range            = "10.45.0.0/16"
  network                  = "${google_compute_network.prod-network.self_link}"
  region                   = "${var.cloudreach_region}"
  description              = "subnet to deploy kubernetes cluster to"
  project                  = "${var.cloudreach_project}"
  private_ip_google_access = "true"
  enable_flow_logs         = "false"

  secondary_ip_range = [{
    "ip_cidr_range" = "10.46.0.0/16"

    "range_name" = "secondary1"
  },
    {
      "ip_cidr_range" = "10.47.0.0/16"

      "range_name" = "secondary2"
    },
  ]
}
