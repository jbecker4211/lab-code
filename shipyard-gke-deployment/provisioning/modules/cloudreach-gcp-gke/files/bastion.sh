#!/bin/bash
sudo yum -y update
sudo yum -y install ansible python-pip yum-cron python
sudo curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
sudo chmod 700 get_helm.sh
sudo ./get_helm.sh
sudo rm get_helm.sh
sudo cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubectl git wget
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce -y
sudo systemctl start docker

