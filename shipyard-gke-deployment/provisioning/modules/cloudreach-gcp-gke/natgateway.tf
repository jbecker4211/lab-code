#### DOCKER INSTANCE INSTANCE

data "template_file" "nat-startup-script" {
  template = "${file("${format("%s/files/nat.sh",path.module)}")}"
}

resource "google_compute_instance" "nat-gateway" {
  name         = "nat-gateway"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  network_interface {
    subnetwork         = "${google_compute_subnetwork.prod_subnetworks.0.self_link}"
    subnetwork_project = "${var.cloudreach_project}"

    access_config {
      network_tier = "PREMIUM"
    }
  }

  boot_disk {
    auto_delete = "true"
    device_name = "nat_boot_disk"

    initialize_params {
      type  = "pd-standard"
      image = "debian-cloud/debian-9"
    }
  }

  service_account {
    email  = "${google_service_account.nat-service-account.email}"
    scopes = ["cloud-platform"]
  }

  metadata_startup_script   = "${data.template_file.nat-startup-script.rendered}"
  allow_stopping_for_update = "true"
  can_ip_forward            = "true"
  description               = "nat instance"
  deletion_protection       = "false"
  project                   = "${var.cloudreach_project}"

  scheduling {
    preemptible         = "false"
    on_host_maintenance = "MIGRATE"
    automatic_restart   = "true"
  }

  metadata = "${var.cloudreach_nat_metadata}"
  tags     = ["bastionmanaged", "external"]
}
