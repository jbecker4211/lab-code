#### FIREWALL RULE FOR SSH ACCESS
resource "google_compute_firewall" "bastion22" {
  name          = "allow-kubectl-bastion"
  network       = "${google_compute_network.bastion-network.self_link}"
  source_ranges = "${var.cloudreach_firewall_source_ranges}"

  allow {
    protocol = "TCP"
    ports    = ["8080"]
  }

  description = "Bastion Instance Access for Kubectl"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["bastion"]
  direction   = "INGRESS"
}

#### FIREWALL RULE FOR BASTION ACCESS
resource "google_compute_firewall" "bastion-access" {
  name          = "bastion-ssh-access"
  network       = "${google_compute_network.prod-network.self_link}"
  source_ranges = ["${var.cloudreach_subnet_ip_cidr_range_bastion}"]

  allow {
    protocol = "TCP"
    ports    = ["22"]
  }

  allow {
    protocol = "icmp"
  }

  description = "Bastion managed instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["bastionmanaged"]
  direction   = "INGRESS"
}

#### FIREWALL RULE FOR EXTERNAL ACCESS BASTION

resource "google_compute_firewall" "external-access" {
  name          = "allow-external"
  network       = "${google_compute_network.bastion-network.self_link}"
  source_ranges = "${var.cloudreach_firewall_source_ranges}"

  allow {
    protocol = "TCP"
    ports    = ["80"]
  }

  allow {
    protocol = "TCP"
    ports    = ["22"]
  }

  allow {
    protocol = "TCP"
    ports    = ["8080"]
  }
allow {
protocol = "TCP"
ports = ["443"]
}
  allow {
    protocol = "ICMP"
  }

  allow {
    protocol = "TCP"
    ports    = ["44134"]
  }

  description = "Bastion Instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["external"]
  direction   = "INGRESS"
}

#### FIREWALL RULE FOR EXTERNAL ACCESS PROD

resource "google_compute_firewall" "external-access-prod" {
  name          = "allow-external-prod"
  network       = "${google_compute_network.prod-network.self_link}"
  source_ranges = "${var.cloudreach_firewall_source_ranges}"

  allow {
    protocol = "TCP"
    ports    = ["1-6000"]
  }

  allow {
    protocol = "ICMP"
  }

  description = "Bastion Instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["external"]
  direction   = "INGRESS"
}
